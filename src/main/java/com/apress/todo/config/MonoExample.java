package com.apress.todo.config;

import com.apress.todo.domain.ToDo;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.publisher.MonoProcessor;
import reactor.core.scheduler.Schedulers;


/**
 * Class MonoExsample
 * todo: описание
 *
 * @author Kaleganov Alexander
 * @since 09 янв. 21
 */
@Configuration
@Log4j2
public class MonoExample {

    @Bean
    public CommandLineRunner commandLineRunner() {
        return args -> {
//
//            WebClient client = WebClient
//                    .builder()
////                    .defaultHeaders(header -> header.setBasicAuth(userName, password))
//                    .baseUrl("http://search.maps.sputnik.ru")
//                    .defaultHeader("Accept", "application/json, text/plain, */*")
//                    .build();
//            WebClient.ResponseSpec uri1 = client.get().uri(uriBuilder -> uriBuilder
//                    .path("/search/addr")
//                    .queryParam("strict", true)
//                    .queryParam("apikey", "5032f91e8da6431d8605-f9c0c9a00357")
//                    .queryParam("q", "Москва, Тверская улица 13")
//                    .build()).retrieve();
//            uri1.bodyToMono(String.class).doOnSubscribe(
//                    e->log.error("пришёл ответ  = {}", e))
//                    .doOnSuccess(e->log.error("succes = {}", e))
//                    .doOnError(e->log.error(e.getMessage(), e))
//                    .doOnTerminate(()->log.error("terminated"))
//                    .block();
//          MonoProcessor<ToDo> promise  = MonoProcessor.create();
//          Mono<ToDo> result = promise.doOnSuccess(p->log.error("MONO >> 😢 ToDo:{}", p.getDescription()))
//                  .doOnTerminate(()->log.error("MONO Terminated {}", "done"))
//                  .doOnError(e->log.error("MONO ERROR {}", e.getMessage(), e))
//                  .subscribeOn(Schedulers.single());
//            promise.onNext( new ToDo("Test 2020"));
////            promise.onError(new IllegalAccessError("произошёл еррор"));
//            result.block();
        };
    }

}
