package com.apress.todo.config;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;
import com.fasterxml.classmate.types.ResolvedArrayType;
import lombok.RequiredArgsConstructor;
import lombok.var;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.ResponseEntity;
import org.springframework.web.method.HandlerMethod;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.spring.web.readers.operation.HandlerMethodResolver;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Предоставляет конфигурацию Swagger по умолчанию.
 */
@Configuration
@EnableSwagger2
@RequiredArgsConstructor
public class SwaggerConfiguration {

    private static final String OAUTH_NAME = "MES-ADP";

    private static final String KEY_TYPE = "JWT";

    private static final String KEY_NAME = "Authorization";

    private static final String PASS_AS = "header";

    private static final String ALLOWED_PATHS = "/api/.*";

    private static final String TITLE = "NLMK MES REST API Documentation";

    private static final String SCOPE = "global";

    private static final String SCOPE_DESCRIPTION = "accessEverything";

    /**
     * Предоставляет бин {@link springfox.documentation.spring.web.plugins}.
     *
     * @return {@link springfox.documentation.spring.web.plugins}.
     */
    @Bean
    public Docket taskApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .securityContexts(Collections.singletonList(securityContext()))
                .securitySchemes(Collections.singletonList(apiKey()))
                .useDefaultResponseMessages(true)
                .useDefaultResponseMessages(true)
                .directModelSubstitute(LocalDate.class, String.class)
                .directModelSubstitute(LocalTime.class, String.class)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.regex(ALLOWED_PATHS))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(TITLE)
                .build();
    }

    private ApiKey apiKey() {
        return new ApiKey(KEY_TYPE, KEY_NAME, PASS_AS);
    }

    private AuthorizationScope[] scopes() {
        return new AuthorizationScope[] {
                new AuthorizationScope(SCOPE, SCOPE_DESCRIPTION)};
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                              .securityReferences(
                                      defaultAuth())
                              .operationSelector(op -> op.requestMappingPattern().matches(ALLOWED_PATHS))
                              .build();
    }

    private List<SecurityReference> defaultAuth() {
        return Arrays.asList(
                new SecurityReference(KEY_TYPE, scopes()),
                new SecurityReference(OAUTH_NAME, scopes()));
    }

}
