package com.apress.todo.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
/**
 * Class ToDo
 * объект списка
 *
 * @author Kaleganov Alexander
 * @since 26 дек. 20
 */
@Data
@Entity
@NoArgsConstructor
@ApiModel("Моделька")
@AllArgsConstructor
public class ToDo {
    public ToDo(@NonNull @NotBlank String description) {
        this.description = description;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty("поле1")
    private Long id;
    @NonNull
    @NotBlank
    @ApiModelProperty("поле2")
    private String description;
    @ApiModelProperty("поле3")
    private LocalDateTime created;
    @ApiModelProperty("поле4")
    private LocalDateTime modified;
    @ApiModelProperty("поле5")
    private boolean completed;

}
