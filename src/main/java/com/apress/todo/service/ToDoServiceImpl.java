package com.apress.todo.service;

import com.apress.todo.domain.ToDo;
import com.apress.todo.repository.CommonRepository;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Class ToDoServiceImpl
 * todo: описание
 *
 * @author Kaleganov Alexander
 * @since 05 янв. 21
 */
@Service
@Slf4j
public class ToDoServiceImpl implements ToDoService<ToDo> {
    ExecutorService executorService = Executors.newFixedThreadPool(1);
    @Autowired
    private CommonRepository commonRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public ToDo save(ToDo domain) {
        return this.commonRepository.saveAndFlush(domain);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveExperement(ToDo domain) {
        var res =  save(domain);
        var newDomain = new
                ToDo();
        newDomain.setDescription("sssssssssssssssssssss");
        executorService.submit(() -> this.save(newDomain));
        log.error("res = {}", res);
        throw new RuntimeException();
    }

    @Override
    public List<ToDo> save(Collection<ToDo> domains) {
        return this.commonRepository.saveAll(domains);
    }

    @Override
    public void delete(Long id) {
        this.commonRepository.deleteById(id);

    }

    @Override
    public ToDo findById(Long id) {
        return this.commonRepository.findById(id).orElse(new ToDo());
    }

    @Override
    public List<ToDo> findAll() {
        return this.commonRepository.findAll();
    }


}
