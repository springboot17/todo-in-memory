package com.apress.todo;

import lombok.Data;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TodoInMemoryApplicationTests {

	@Test
	void contextLoads() {
		int i = 0;
		Integer k = 0;
		increment(i, k);
		System.out.println(i);
		System.out.println(k);
	}

	private static void increment(int i, Integer k) {
		i = i + 1;
		k = k + Integer.valueOf(1);
	}

	private static void inc (To to ) {
		to = new To();
	}
	@Data
	private static class To {
		Integer n ;
	}
}
